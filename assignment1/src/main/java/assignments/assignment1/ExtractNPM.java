package assignments.assignment1;

import java.util.Scanner;

public class ExtractNPM {
    public static int ubah(String suatu){       // Method mengubat String ke Int
        int k = Integer.parseInt(suatu);
        return k;
    }
    public static int kodenpm(String npm){      // Method untuk mencari kode NPM melalui 13 digit NPM
        String ambil = npm.substring(0,13); 
        int kodenpm = 0;
        for (int i = 0; i < (ambil.length())/2; i++){
            char c = ambil.charAt(i);
            char v = ambil.charAt(ambil.length()- i - 1);
            String s = Character.toString(c);
            String m = Character.toString(v);
            kodenpm = kodenpm + (ubah(s) * ubah(m));
        }
        return kodenpm;
    } 
    public static int kodenpm2(String npm){     // Method untuk mencari kode npm melalui hasil dari penjumlahan dan perkalian 13 digit NPM
        int kodenpm = 0;
        for (int i = 0; i < (npm.length()/2); i++){
            char c = npm.charAt(i);
            char v = npm.charAt(npm.length()-i-1);
            String s = Character.toString(c);
            String m = Character.toString(v);
            kodenpm = kodenpm + (ubah(s) + ubah(m));
        }
        return kodenpm;
    }
    public static boolean validate(long npm) {
        // Method validasi NPM
        boolean palid = false;                  // Variable boolean
        String panjang = String.valueOf(npm);   // Mengubah variable long ke String
        char np = panjang.charAt(13);           // Mengambil Char kode NPM
        String kodee = Character.toString(np);  // Mengubah kode npm dari Char ke String
        int tahunlahir = Integer.parseInt(panjang.substring(8,12)); // Variable tahunlahir
        int tahunmasuk = Integer.parseInt(panjang.substring(0,2));  // Variable tahunmasuk
        int ngodenpm = kodenpm(panjang);                            // Variable untuk mengecek kode NPM dengan method sebelumnya
        String ngodeke2 = String.valueOf(ngodenpm);
        if (panjang.length() == 14){                                // Conditional statement pertama             
            if (panjang.charAt(2) == '0'){                          // Conditional statement kedua   
                if (panjang.charAt(3) == '1' || panjang.charAt(3) == '2' || panjang.charAt(3) == '3'){  
                    if ((tahunmasuk + 2000) - tahunlahir >= 15){    // Conditional statement ketiga
                        if (kodenpm2(ngodeke2) == ubah(kodee)){     // Conditional statement keempat
                            palid = true;
                        }
                        else {
                            if (kodenpm2(String.valueOf(ngodenpm)) == ubah(kodee)){
                                palid = true;
                            }
                            else {
                                palid = false;
                            }
                        }
                    }
                    else {
                        palid = false;
                    }
                }
            }
            else if (panjang.charAt(2) == '1'){                     // Conditional statement kedua 
                if (panjang.charAt(3) == '1' || panjang.charAt(3) == '2'){
                    if ((tahunmasuk + 2000) - tahunlahir >= 15){    // Conditional statement ketiga
                        if (ngodenpm == ubah(kodee)){               // Conditional statemnet keempat
                            palid = true;
                        }
                        else {
                            if (kodenpm(String.valueOf(ngodenpm)) == ubah(kodee)){
                                palid = true;
                            }
                            else {
                                palid = false;
                            }
                        }
                    }
                    else {
                        palid = false;
                    }
                }
            }
            else {
                palid = false;
            }
        }
        else {
            palid = false;
        }
        return palid;   // mereturn boolean
    }

    public static String extract(long npm) {
        // Method untuk mengekstrak NPM jika valid
        String panjang = String.valueOf(npm);   // NPM dijadikan String
        String jurusan = "";                    // Variable jurusan
        if (panjang.charAt(2) == '0'){          // Conditional Statement jurusan
            if (panjang.charAt(3) == '1'){
                jurusan = "Ilmu Komputer"; 
            }
            else if (panjang.charAt(3) == '2'){
                jurusan = "Sistem Informasi"; 
            }
            else if (panjang.charAt(3) == '3'){
                jurusan = "Teknologi Informasi";
            }
            else {
                jurusan = "";
            }
        }
        else if (panjang.charAt(2) == '1'){
            if (panjang.charAt(3) == '1'){
                jurusan = "Teknik Telekomunikasi";
            }
            else if (panjang.charAt(3) == '2'){
                jurusan = "Teknik Elektro";
            }
            else {
                jurusan = "";
            }
        }
        else {
            jurusan = "";
        }
        String lahir = "";                              // Variable tanggal lahir
        String tahunlahir = panjang.substring(4,12);
        int tahunmasuk = Integer.parseInt(panjang.substring(0,2)) + 2000;   // Variable tahun masuk
        for (int i = 0; i < tahunlahir.length(); i++){  // Looping untuk membuat tanggal lahir
            char c = tahunlahir.charAt(i);
            String s = Character.toString(c);
            if (i == 1 || i == 3){
                lahir = (lahir + s + "-"); 
            }
            else if ( i == 2 && c == '0') {
                continue;
            }
            else {
                lahir = lahir + s;
            }
        }
        // Variable String untuk data yang diekstrak
        String datanya = ("Tahun masuk: " + tahunmasuk + "\n" + "Jurusan: " + jurusan + "\n" + "Tanggal Lahir: " + lahir);
        return datanya;
    }

    public static void main(String args[]) {    // Permulaan Progam
        Scanner input = new Scanner(System.in);
        boolean exitFlag = false;
        while (!exitFlag) {
            System.out.print("Masukkan NPM : ");
            long npm = input.nextLong();
            String p = String.valueOf(npm);
            if (npm < 0) {
                exitFlag = true;
                break;
            }
            else if (p.charAt(0) == '0'){
                continue;
            }
            else {
                if (validate(npm) == true){         // Conditional Statement validasi NPM apabila valid
                    System.out.print(extract(npm)); // Diekstrak
                }
                else if (!validate(npm)){           // Conditional Statement validasi NPM yang tidak valid
                    System.out.println("NPM tidak valid!");
                }
                else {
                    continue;
                }
            }
        }
        input.close();
    }
}