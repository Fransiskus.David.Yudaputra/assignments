package assignments.assignment2;


public class Mahasiswa {
    private static final int maksMK = 10; 
    private MataKuliah[] mataKuliah = new MataKuliah[maksMK];
    private int numMatkul = 0;
    private String[] masalahIRS = new String[19];
    private int totalmasalah = 0;
    private int totalSKS;
    private String nama;
    private String jurusan;
    private long npm;

    // Constructor
    public Mahasiswa(String nama, long npm){
        this.nama = nama;
        this.npm = npm;
    }

    // Setter
    public void setNama (String nama) {
        this.nama = nama;
    }
    public void setNpm (long npm) {
        this.npm = npm;
    }

    // Getter
    public String getNama () {
        return this.nama;
    }
    public long getNpm () {
        return this.npm;
    }
    public String getJurusan() {
        String panjang = String.valueOf(this.npm);
        if (panjang.charAt(3) == '1') {
            this.jurusan = "Ilmu Komputer";
        }
        else if (panjang.charAt(3) == '2') {
            this.jurusan = "Sistem Informasi";
        }
        return this.jurusan;
    }
    public int getTotalmatkul() {
        return this.numMatkul;
    }
    public int getTotalSKS() {
        return this.totalSKS;
    }
    public String getdaftarmataKuliah() {
        StringBuilder str = new StringBuilder();
        for (int i = 0; i < this.numMatkul; i++) {
            str.append(this.mataKuliah[i].toString() + "\n"); 
        }
        return str.toString();
    }
    
    // Method addMatkul
    public void addMatkul(MataKuliah mataKuliah){
        String panjang = String.valueOf(this.npm);
        String jurus = "";
        if (panjang.charAt(3) == '1') {
            jurus = "IK";
        }
        else if (panjang.charAt(3) == '2') {
            jurus = "SI";
        }
        if (mataKuliah.getKode().equals("CS")) {
            this.mataKuliah[this.numMatkul] = mataKuliah;
            this.totalSKS += mataKuliah.getSks();
            mataKuliah.addMahasiswa(this);
            this.numMatkul ++; 
        }
        else if (this.numMatkul == maksMK) {
            System.out.println("[DITOLAK] Maksimal mata kuliah yang diambil hanya 10");
        }
        else if (!mataKuliah.getKode().equals(jurus)) {
            System.out.println("[DITOLAK] Mahasiswa tidak bisa mengambil mata kuliah ini");
        }
        else if (this.totalSKS >= 24) {
            System.out.println("[DITOLAK] SKS Mahasiswa sudah penuh");
        }
        
        else {
            this.mataKuliah[this.numMatkul] = mataKuliah;
            this.totalSKS += mataKuliah.getSks();
            mataKuliah.addMahasiswa(this);
            this.numMatkul ++; 
        }
    }

    // Method dropMatkul
    public void dropMatkul(MataKuliah mataKuliah){
        for (int i = 0; i < this.numMatkul; i++) {
            if (this.mataKuliah[i] == mataKuliah) {
                this.mataKuliah[i] = this.mataKuliah[numMatkul-1];
                this.mataKuliah[numMatkul-1] = null;
                this.totalSKS -= mataKuliah.getSks();
                this.numMatkul--;
                mataKuliah.dropMahasiswa(this); 
                return;
            } 
        }
        System.out.println("[Ditolak] Mata Kuliah " + mataKuliah.getNama() + " belum diambil sebelumnya");
    }

    // Method cekIRS
    public void cekIRS(){
        String panjang = String.valueOf(this.npm);
        String jurusan = "";
        if (panjang.charAt(3) == '1') {
            jurusan = "IK";
        }
        else if (panjang.charAt(3) == '2') {
            jurusan = "SI";
        }
        for (int i = 0; i < this.numMatkul; i++) {
            if (this.mataKuliah[i].getKode().equals("CS")){
                continue;
            }
            else if (!this.mataKuliah[i].getKode().equals(jurusan)) {
                this.masalahIRS[this.totalmasalah] = ("Mata Kuliah " + this.mataKuliah[i].getNama() + " tidak dapat diambil jurusan " + this.jurusan);
                this.totalmasalah++;
            }
            else {
                System.out.println("Mahasiswa tidak bermasalah");
            }

        }
    }

    public String getIRS() {
        StringBuilder str = new StringBuilder();
        if (this.totalmasalah == 0) {
            str.append("IRS tidak bermasalah");
        }
        else {
            for (int i = 0; i < this.totalmasalah; i++) {
                str.append(this.masalahIRS[i] + "\n");
            }
        }
        return str.toString();
    }

    // Printing
    public String toString() {
        return String.format("%s" ,this.nama);
    }
  

}

