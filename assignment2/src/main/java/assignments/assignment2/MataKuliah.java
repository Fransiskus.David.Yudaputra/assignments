package assignments.assignment2;

public class MataKuliah {
    private String kode;
    private String nama;
    private int sks;
    private int kapasitas;
    private Mahasiswa[] daftarMahasiswa;
    private int numMahasiswa = 0;

    // Constructor
    public MataKuliah(String kode, String nama, int sks, int kapasitas){
        this.kode = kode;
        this.nama = nama;
        this.sks = sks;
        this.kapasitas = kapasitas;

        daftarMahasiswa = new Mahasiswa[kapasitas];
        /* TODO: implementasikan kode Anda di sini */
    }

    // Setter
    public void setKode(String kode) {
        this.kode = kode;
    }
    public void setNama(String nama) {
        this.nama = nama;
    }
    public void setSks(int sks) {
        this.sks = sks;
    }
    public void setKapasitas(int kapasitas) {
        this.kapasitas = kapasitas;
    }

    // Getter
    public String getKode() {
        return this.kode;
    }
    public String getNama() {
        return this.nama;
    }
    public int getSks() {
        return this.sks;
    }
    public int getKapasitas() {
        return this.kapasitas;
    }
    public int getnumMahasiswa () {
        return this.numMahasiswa;
    }
    public String getdftarMhs() {
        StringBuilder str = new StringBuilder();
        if (this.numMahasiswa == 0) {
            str.append("Belum ada Mahasiswa yang mengambil Mata Kuliah ini");
        }
        else {
            for (int i = 0; i < this.numMahasiswa; i++) {
                str.append(this.daftarMahasiswa[i].toString() + "\n"); 
            }
        }
        
        return str.toString();
    }

    public void addMahasiswa(Mahasiswa mahasiswa) {
        if (this.numMahasiswa == this.kapasitas) {
            System.out.println("Mata kuliah ini sudah penuh");
        }
        else {
            this.daftarMahasiswa[this.numMahasiswa] = mahasiswa;
            this.numMahasiswa++;
        }
        
        /* TODO: implementasikan kode Anda di sini */
        
    }

    public void dropMahasiswa(Mahasiswa mahasiswa) {
        for (int i = 0; i < this.numMahasiswa; i++) {
            if (daftarMahasiswa[i] == mahasiswa) {
                this.daftarMahasiswa[i] = this.daftarMahasiswa[numMahasiswa-1];
                this.daftarMahasiswa[numMahasiswa-1] = null;
                this.numMahasiswa--;
            }
        }
        /* TODO: implementasikan kode Anda di sini */

    }

    public String toString() {
        /* TODO: implementasikan kode Anda di sini */
        return String.format("%s" ,this.nama);
    }
}
