package assignments.assignment3;

public class Dosen extends ElemenFasilkom {

    private MataKuliah mataKuliah;
    // Constructor
    public Dosen(String nama) {
        super(nama,"dosen");
    }
    // Getter
    public String getMatkul() {
        return this.getMatkul();
    }

    // Method mengajar dan drop matkul
    public void mengajarMataKuliah(MataKuliah matakKuliah) {
        if (this.mataKuliah != null) {
            System.out.println("[DITOLAK] " + this.getNama() + " sudah mengajar mata kuliah " + this.mataKuliah.getNama());
            return;
        }
        else if (matakKuliah.getDDosen() != null){
            System.out.println("[DITOLAK] " + matakKuliah.getDosen() + " sudah mengajar mata kuliah " + matakKuliah.getNama());
            return;
        }
        this.mataKuliah = matakKuliah;
        matakKuliah.addDosen(this);
        System.out.println(this.getNama() + " mengajar mata kuliah " + matakKuliah.getNama());
            
    }

    public void dropMataKuliah() {
        if (this.mataKuliah == null) {
            System.out.println("[DITOLAK] " + this.getNama() + " sedang tidak mengajar mata kuliah apapun");
            return;
        }
        System.out.println(this.getNama() + " berhenti mengajar " + this.mataKuliah);
        this.mataKuliah.dropDosen();
        this.mataKuliah = null;
        
    }
}