package assignments.assignment3;

// Super Class ElemenFasilkom
public abstract class ElemenFasilkom {
    

    private String tipe;
    
    private String nama;

    private int friendship;

    private ElemenFasilkom[] telahMenyapa = new ElemenFasilkom[100];
    private int orangke = 0;
    
    // Constructor
    public ElemenFasilkom(String nama, String tipe) {
        this.nama = nama;
        this.tipe = tipe;
    }

    // Getter 
    public String getNama () {
        return this.nama;
    }
    public String getTipe() {
        return this.tipe;
    }
    public int getFriendship() {
        return this.friendship;
    }
    public int getDahnyapa() {
        return this.orangke;
    }

    // Setter
    public void setNama(String nama) {
        this.nama = nama;
    }
    public void setTipe(String tip) {
        this.tipe = tip;
    }
    public void setFriendship(int friend) {
        this.friendship = friend;
    }

    // Method Menyapa
    public void menyapa(ElemenFasilkom elemenFasilkom) {
        for (int i = 0; i < telahMenyapa.length; i++) {
            if (telahMenyapa[i] == elemenFasilkom) {
                System.out.println("[DITOLAK] " + this.getNama() + " telah menyapa " + elemenFasilkom.getNama() + " hari ini" );
                return;
            }
        }
        this.telahMenyapa[this.orangke] = elemenFasilkom;
        this.orangke++;
        System.out.println(this.getNama() + " menyapa dengan " + elemenFasilkom.getNama());
    }

    // Method reset
    public void resetMenyapa() {
        this.telahMenyapa = new ElemenFasilkom[100];
        this.orangke = 0;
    }

    // Method beli makanan
    public void membeliMakanan(ElemenFasilkom pembeli, ElemenFasilkom penjual, String namaMakanan) {
        ElemenKantin s = (ElemenKantin) penjual;
        if (s.getMakan(namaMakanan).equals("none")){
            System.out.println("[DITOLAK] " + s.getNama() + " tidak menjual " + namaMakanan);
        }
        else {
            System.out.println(pembeli.getNama() + " berhasil membeli " + s.getMakan(namaMakanan) + " dengan harga " + s.getHarga(namaMakanan));
        }
    }

    // Method toString
    public String toString() {
        return String.format("%s", this.getNama());
    }

    // Method untuk membandingkan
    public static int Bandingkan(ElemenFasilkom p1, ElemenFasilkom p2) {
        if (p1.getFriendship() != p2.getFriendship()) {
            return p2.getFriendship() - p1.getFriendship();
        }
        return p1.getNama().compareTo(p2.getNama());
    }
}