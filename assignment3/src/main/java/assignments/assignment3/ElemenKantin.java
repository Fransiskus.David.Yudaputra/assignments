package assignments.assignment3;

public class ElemenKantin extends ElemenFasilkom {
    
    private Makanan[] daftarMakanan = new Makanan[10];
    private int jumlahmakan = 0;
    // Constructor
    public ElemenKantin(String nama) {
        super(nama, "kantin");
    }

    // Getter
    public String getMakan(String nama) {
        for (int i = 0; i < jumlahmakan; i++) {
            if(daftarMakanan[i].getNama().equals(nama)) {
                return daftarMakanan[i].getNama();
            }
        }
        return "none";
    }

    public long getHarga(String nama) {
        for (int i = 0; i < jumlahmakan; i++) {
            if (daftarMakanan[i].getNama().equals(nama)){
                return daftarMakanan[i].getHarga();
            }
        }
        return 0;
    }

    // Setter
    public void setMakanan(String nama, long harga) {
        for (int i = 0; i < this.jumlahmakan; i++){
            if (this.daftarMakanan[i].getNama().equals(nama)){
                System.out.println("[DITOLAK] " + nama + " sudah pernah terdaftar");
                return;
            }
        }
        if (this.jumlahmakan == daftarMakanan.length) {
            System.out.println("[DITOLAK] jumlah jualan sudah penuh");
            return;
        }
        this.daftarMakanan[this.jumlahmakan] = new Makanan(nama, harga);
        this.jumlahmakan++;
        System.out.println(this.getNama() + " telah mendaftarkan makanan " + nama + " dengan harga " + harga);
    }
}