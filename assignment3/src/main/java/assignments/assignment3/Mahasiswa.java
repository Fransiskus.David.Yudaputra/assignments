package assignments.assignment3;

public class Mahasiswa extends ElemenFasilkom {
    
    private static final int maksMK = 10; 
    private MataKuliah[] mataKuliah = new MataKuliah[maksMK];
    private int numMatkul = 0;
    
    private long npm;

    private String tanggalLahir;
    
    private String jurusan;
    // Constructor
    public Mahasiswa(String nama, long npm) {
        super(nama, "mahasiswa");
        this.npm = npm;
    }

    // Getter
    public long getNpm () {
        return this.npm;
    }

    public int getTotalmatkul() {
        return this.numMatkul;
    }

    public String getdaftarmataKuliah() {
        StringBuilder str = new StringBuilder();
        for (int i = 0; i < this.numMatkul; i++) {
            str.append(this.mataKuliah[i].toString() + "\n"); 
        }
        return str.toString();
    }

    // Method addmatkul
    public void addMatkul(MataKuliah mataKuliah) {
        for (int i = 0; i < numMatkul; i++) {
            if (this.mataKuliah[i] == mataKuliah){
                System.out.println("[DITOLAK] " + mataKuliah.getNama() + " telah diambil sebelumnya");
                return;
            }
        }
        if (mataKuliah.getKapasitas() == mataKuliah.getNumMaha()) {
            System.out.println("[DITOLAK] " + mataKuliah.getNama() + " telah penuh kapasitasnya");
            return;
        }
        this.mataKuliah[this.numMatkul] = mataKuliah;
        mataKuliah.addMahasiswa(this);
        this.numMatkul ++; 
        System.out.println(this.getNama() + " berhasil menambahkan mata kuliah " + mataKuliah.getNama());
    }

    // Method dropmatkul
    public void dropMatkul(MataKuliah mataKuliah) {
        for (int i = 0; i < this.numMatkul; i++) {
            if (this.mataKuliah[i] == mataKuliah) {
                this.mataKuliah[i] = this.mataKuliah[numMatkul-1];
                this.mataKuliah[numMatkul-1] = null;
                this.numMatkul--;
                mataKuliah.dropMahasiswa(this);
                System.out.println(this.getNama() + " berhasil drop mata kuliah " + mataKuliah.getNama()); 
                return;
            } 
        }
        System.out.println("[Ditolak] " + mataKuliah.getNama() + " belum pernah diambil");
    }

    // Method extraction
    public String extractTanggalLahir(long npm) {
        String panjang = String.valueOf(npm);   // NPM dijadikan String
        String tahunlahir = panjang.substring(4,12);
        for (int i = 0; i < tahunlahir.length(); i++){  // Looping untuk membuat tanggal lahir
            char c = tahunlahir.charAt(i);
            String s = Character.toString(c);
            if (i == 1 || i == 3){
                this.tanggalLahir = (this.tanggalLahir + s + "-"); 
            }
            else {
                this.tanggalLahir = this.tanggalLahir + s;
            }
        }
        return this.tanggalLahir;
    }

    public String extractJurusan(long npm) {
        String panjang = String.valueOf(this.npm);
        if (panjang.charAt(3) == '1') {
            this.jurusan = "Ilmu Komputer";
        }
        else if (panjang.charAt(3) == '2') {
            this.jurusan = "Sistem Informasi";
        }
        return this.jurusan;
    }
}