package assignments.assignment3;

import java.util.Arrays;
import java.util.Scanner;


public class Main {

    private ElemenFasilkom[] daftarElemenFasilkom = new ElemenFasilkom[100];

    private MataKuliah[] daftarMataKuliah = new MataKuliah[100];

    private int totalMataKuliah = 0;

    private int totalElemenFasilkom = 0; 
    private Scanner input = new Scanner(System.in);

    // Getter Matkul
    private MataKuliah getmatkul(String nama) {
        for (int i = 0; i < totalMataKuliah; i++){
            if (daftarMataKuliah[i].getNama().equals(nama)){
                return daftarMataKuliah[i];
            }
        }
        return null;
    }

    // Method addMahasiswa
    private void addMahasiswa(String nama, long npm) {
        daftarElemenFasilkom[totalElemenFasilkom] = new Mahasiswa(nama, npm);
        totalElemenFasilkom++;
        System.out.println(nama + " berhasil ditambahkan");
    }

    // Method addDosen
    private void addDosen(String nama) {
        daftarElemenFasilkom[totalElemenFasilkom] = new Dosen(nama);
        totalElemenFasilkom++;
        System.out.println(nama + " berhasil ditambahkan");
    }

    // Method addKantin
    private void addElemenKantin(String nama) {
        daftarElemenFasilkom[totalElemenFasilkom] = new ElemenKantin(nama);
        totalElemenFasilkom++;
        System.out.println(nama + " berhasil ditambahkan");
    }

    // Method Menyapa
    private void menyapa(String objek1, String objek2) {
        ElemenFasilkom s = null;
        Mahasiswa m = new Mahasiswa("", 0);
        Dosen d = new Dosen("");
        // Jika objek menyapa diri sendiri
        if (objek1.equals(objek2)) {
            System.out.println("[DITOLAK] Objek yang sama tidak bisa saling menyapa");
        }
        // Kalau objek tidak menyapa diri sendiri
        else {
            for (int i = 0; i < totalElemenFasilkom; i++){
                if (daftarElemenFasilkom[i].getNama().equals(objek1)) {
                    // Conditional Statement mahasiswa
                    if (daftarElemenFasilkom[i].getTipe().equals("mahasiswa")) {
                        m = (Mahasiswa) daftarElemenFasilkom[i];
                        for (int j = 0; j < totalElemenFasilkom; j++) {
                            if (daftarElemenFasilkom[j].getNama().equals(objek2)) {
                                if (daftarElemenFasilkom[j].getTipe().equals("dosen")) {
                                    m.menyapa(daftarElemenFasilkom[j]);
                                    m.setFriendship(s.getFriendship() + 2);
                                }
                                else {
                                    m.menyapa(daftarElemenFasilkom[j]);
                                }
                            }
                        }
                    }
                    // Conditional Statement dosen
                    else if (daftarElemenFasilkom[i].getTipe().equals("dosen")) {
                        d = (Dosen) daftarElemenFasilkom[i];
                        for (int j = 0; j < totalElemenFasilkom; j++) {
                            if (daftarElemenFasilkom[j].getNama().equals(objek2)) {
                                if (daftarElemenFasilkom[j].getTipe().equals("mahasiswa")){
                                    d.menyapa(daftarElemenFasilkom[j]);
                                    d.setFriendship(d.getFriendship() + 2);
                                }
                                else {
                                    d.menyapa(daftarElemenFasilkom[j]);
                                }
                            }
                        }
                    }
                    // Conditional Statement selain itu
                    else {
                        s = (ElemenFasilkom) daftarElemenFasilkom[i];
                        for (int j = 0; j < totalElemenFasilkom; j++) {
                            if (daftarElemenFasilkom[j].getNama().equals(objek2)) {
                                s.menyapa(daftarElemenFasilkom[j]);
                            }
                        }
                    }
                }
            }
        }
    }

    // Method addMakanan
    private void addMakanan(String objek, String namaMakanan, long harga) {
        ElemenKantin s = new ElemenKantin("");
        for (int i = 0; i < totalElemenFasilkom; i++) {
            if (daftarElemenFasilkom[i].getNama().equals(objek)) {
                if (daftarElemenFasilkom[i].getTipe().equals("kantin")) {
                    s = (ElemenKantin) daftarElemenFasilkom[i];
                    s.setMakanan(namaMakanan, harga);
                    return;
                }
                else {
                    System.out.println("[DITOLAK] " + objek + " bukan merupakan elemen kantin");
                    return;
                }
            }
        }
    }

    // Method MembeliMakanan
    private void membeliMakanan(String objek1, String objek2, String namaMakanan) {
        ElemenKantin k = new ElemenKantin("");
        ElemenFasilkom s = null;
        // Cek apakah objek1 == objek2
        if (objek1.equals(objek2)) {
            System.out.println("Elemen kantin tidak bisa membeli makanan sendiri");
        }
        // Cek elemenkantin
        for (int j = 0; j < totalElemenFasilkom; j++){
            if (daftarElemenFasilkom[j].getNama().equals(objek2)) {
                if (daftarElemenFasilkom[j].getTipe().equals("kantin")) {
                    k = (ElemenKantin) daftarElemenFasilkom[j];
                }
                else {
                    System.out.println("[DITOLAK] Hanya elemen kantin yang dapat menjual makanan");
                    return;
                }
            }
        }
        // downcasting elemen pembeli
        for (int i = 0; i < totalElemenFasilkom; i++) {
            if (daftarElemenFasilkom[i].getNama().equals(objek1)) {
                s = daftarElemenFasilkom[i];
            }
        }
        s.membeliMakanan(s, k, namaMakanan);
        s.setFriendship(s.getFriendship() + 1);
    }

    // Method CreateMatkul
    private void createMatkul(String nama, int kapasitas) {
        daftarMataKuliah[totalMataKuliah] = new MataKuliah(nama, kapasitas);
        totalMataKuliah++;
        System.out.println(nama + " berhasil ditambahkan dengan kapasitas " + kapasitas);
    }

    // Method addMatkul
    private void addMatkul(String objek, String namaMataKuliah) {
        Mahasiswa s = new Mahasiswa("",0);
        for (int i = 0; i < totalElemenFasilkom; i++) {
            if (daftarElemenFasilkom[i].getNama().equals(objek)) {
                if (daftarElemenFasilkom[i].getTipe().equals("mahasiswa")) {
                    s = (Mahasiswa) daftarElemenFasilkom[i];
                    s.addMatkul(getmatkul(namaMataKuliah));
                    return;
                }
                else {
                    System.out.println("[DITOLAK] Hanya mahasiswa yang dapat menambahkan matkul");
                    return;
                }
            }
        }
    }

    // Method DropMatkul
    private void dropMatkul(String objek, String namaMataKuliah) {
        Mahasiswa s = new Mahasiswa("", 0);
        for (int i = 0; i < totalElemenFasilkom; i++){
            if (daftarElemenFasilkom[i].getNama().equals(objek)) {
                if(daftarElemenFasilkom[i].getTipe().equals("mahasiswa")) {
                    s = (Mahasiswa) daftarElemenFasilkom[i];
                    s.dropMatkul(getmatkul(namaMataKuliah));
                    return;
                }
                else {
                    System.out.println("[DITOLAK] Hanya mahasiswa yang dapat drop matkul");
                    return;
                }
            }
        }
    }

    // Method MengajarMatkul
    private void mengajarMatkul(String objek, String namaMataKuliah) {
        Dosen s = new Dosen("");
        for (int i = 0; i < totalElemenFasilkom; i++) {
            if (daftarElemenFasilkom[i].getNama().equals(objek)) {
                if (daftarElemenFasilkom[i].getTipe().equals("dosen")) {
                    s = (Dosen) daftarElemenFasilkom[i];
                    s.mengajarMataKuliah(getmatkul(namaMataKuliah));
                    return;
                }
                else {
                    System.out.println("[DITOLAK] Hanya dosen yang dapat mengajar matkul");
                    return;
                }
            }
        }
    }

    // Method berhentiMengajar
    private void berhentiMengajar(String objek) {
        Dosen s = new Dosen("");
        for (int i = 0; i < totalElemenFasilkom; i++) {
            if (daftarElemenFasilkom[i].getNama().equals(objek)) {
                if (daftarElemenFasilkom[i].getTipe().equals("dosen")) {
                    s = (Dosen) daftarElemenFasilkom[i];
                    s.dropMataKuliah();
                    return; 
                }
                else {
                  System.out.println("[DITOLAK] Hanya dosen yang dapat berhenti mengajar");
                    return;
                } 
            }
        }
    }

    // Method ringkasanMhs
    private void ringkasanMahasiswa(String objek) {
        Mahasiswa s = new Mahasiswa("", 0);
        for (int i = 0; i < totalElemenFasilkom; i++){
            if (daftarElemenFasilkom[i].getNama().equals(objek)) {
               if (daftarElemenFasilkom[i].getTipe().equals("mahasiswa")) {
                    s = (Mahasiswa) daftarElemenFasilkom[i];
                    System.out.println("Nama: " + s.getNama());
                    System.out.println("Tanggal Lahir: " + s.extractTanggalLahir(s.getNpm()));
                    System.out.println("Jurusan " + s.extractJurusan(s.getNpm()));
                    System.out.println("Daftar Mata Kuliah " + s.getdaftarmataKuliah());
                    return;
                }
                else {
                    System.out.println("[DITOLAK] " + objek + " bukan merupakan seorang mahasiswa");
                    return;
                } 
            }
            
        }
        
    }

    // Method RingkasanMatkul
    private void ringkasanMataKuliah(String namaMataKuliah) {
        MataKuliah m = getmatkul(namaMataKuliah);
        System.out.println("Nama mata kuliah: " + namaMataKuliah);
        System.out.println("Jumlah mahasiswa: " + m.getNumMaha());
        System.out.println("Kapasitas: " + m.getKapasitas());
        System.out.println("Dosen pengajar: " + m.getDosen());
        System.out.println("Daftar mahasiswa yang mengambil mata kuliah ini:" + "\n"  + m.getdftarMhs());
    }

    // Method Nextday
    private void nextDay() {
        int cek = (totalElemenFasilkom-1)/2;
        // Mengecek
        for (int i = 0; i < totalElemenFasilkom; i++) {
            if (daftarElemenFasilkom[i].getDahnyapa() >= cek) {
                daftarElemenFasilkom[i].setFriendship(daftarElemenFasilkom[i].getFriendship() + 10);
                daftarElemenFasilkom[i].resetMenyapa();
                if (daftarElemenFasilkom[i].getFriendship() >= 100) {
                    daftarElemenFasilkom[i].setFriendship(100);
                }
            }
            else if (daftarElemenFasilkom[i].getDahnyapa() < cek) {
                daftarElemenFasilkom[i].setFriendship(daftarElemenFasilkom[i].getFriendship() - 5);
                daftarElemenFasilkom[i].resetMenyapa();
                if (daftarElemenFasilkom[i].getFriendship() < 0) {
                    daftarElemenFasilkom[i].setFriendship(0);
                }
            }
        }
        // Printing
        System.out.println("Hari telah berakhir dan nilai friendship telah diupdate");
        friendshipRanking();
    }

    // Method Ranking nilai keramahan
    private void friendshipRanking() {
        ElemenFasilkom[] frds = new ElemenFasilkom[100];
        for (int i = 0; i < totalElemenFasilkom; i++) {
            if (daftarElemenFasilkom[i] == null) {
                break;
            }
            frds[i] = daftarElemenFasilkom[i];
        }
        try {
            Arrays.sort(frds, ElemenFasilkom::Bandingkan);
        }
        catch (NullPointerException e) {
        }
        
        for(int i = 0; i < totalElemenFasilkom; i++) {
            System.out.println((i+1) + ". " + frds[i].getNama() + "(" + frds[i].getFriendship() + ")");
        }
    }

    // Method progamEnd
    public void programEnd() {
        System.out.println("Program telah berakhir. Berikut nilai terakhir dari friendship pada fasilkom");
        friendshipRanking();
    }

    // Method programnya
    private void run() {
        while (true) {
            try {
                String in = input.nextLine();
                if (in.split(" ")[0].equals("ADD_MAHASISWA")) {
                    addMahasiswa(in.split(" ")[1], Long.parseLong(in.split(" ")[2]));
            }   else if (in.split(" ")[0].equals("ADD_DOSEN")) {
                    addDosen(in.split(" ")[1]);
            }   else if (in.split(" ")[0].equals("ADD_ELEMEN_KANTIN")) {
                    addElemenKantin(in.split(" ")[1]);
            }   else if (in.split(" ")[0].equals("MENYAPA")) {
                    menyapa(in.split(" ")[1], in.split(" ")[2]);
            }   else if (in.split(" ")[0].equals("ADD_MAKANAN")) {
                    addMakanan(in.split(" ")[1], in.split(" ")[2], Long.parseLong(in.split(" ")[3]));
            }   else if (in.split(" ")[0].equals("MEMBELI_MAKANAN")) {
                    membeliMakanan(in.split(" ")[1], in.split(" ")[2], in.split(" ")[3]);
            }   else if (in.split(" ")[0].equals("CREATE_MATKUL")) {
                    createMatkul(in.split(" ")[1], Integer.parseInt(in.split(" ")[2]));
            }   else if (in.split(" ")[0].equals("ADD_MATKUL")) {
                    addMatkul(in.split(" ")[1], in.split(" ")[2]);
            }   else if (in.split(" ")[0].equals("DROP_MATKUL")) {
                    dropMatkul(in.split(" ")[1], in.split(" ")[2]);
            }   else if (in.split(" ")[0].equals("MENGAJAR_MATKUL")) {
                    mengajarMatkul(in.split(" ")[1], in.split(" ")[2]);
            }   else if (in.split(" ")[0].equals("BERHENTI_MENGAJAR")) {
                    berhentiMengajar(in.split(" ")[1]);
            }   else if (in.split(" ")[0].equals("RINGKASAN_MAHASISWA")) {
                    ringkasanMahasiswa(in.split(" ")[1]);
            }   else if (in.split(" ")[0].equals("RINGKASAN_MATKUL")) {
                    ringkasanMataKuliah(in.split(" ")[1]);
            }   else if (in.split(" ")[0].equals("NEXT_DAY")) {
                    nextDay();
            }   else if (in.split(" ")[0].equals("PROGRAM_END")) {
                    programEnd();
                    break;
            }
            }
            catch (ArrayIndexOutOfBoundsException e) {
                System.out.println("[DITOLAK] perintah tidak sah");
            }
        }
    }

    public static void main(String[] args) {
        Main progam = new Main();
        progam.run();
    }
}