package assignments.assignment3;

public class Makanan {


    private String nama;

    private long harga;

    //Constructor
    public Makanan(String nama, long harga) {
        this.nama = nama;
        this.harga = harga;
    }

    // Getter
    public String getNama() {
        return this.nama;
    }

    public long getHarga() {
        return this.harga;
    }

    // Setter
    public void setNama(String name) {
        this.nama = name;
    }

    public void setHarga(long hargas) {
        this.harga = hargas;
    }

    public String toString() {
        return String.format("%s", this.getNama());
    }
}