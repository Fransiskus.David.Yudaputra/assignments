package assignments.assignment3;

public class MataKuliah {

    private String nama;
    
    private int kapasitas;

    private Dosen dosen = null;

    private Mahasiswa[] daftarMahasiswa;
    private int numMahasiswa = 0;
    // Constructor
    public MataKuliah(String nama, int kapasitas) {
        this.nama = nama;
        this.kapasitas = kapasitas;

        daftarMahasiswa = new Mahasiswa[kapasitas];
    }

    // Getter 
    public String getNama() {
        return this.nama;
    }

    public int getKapasitas() {
        return this.kapasitas;
    }

    public int getNumMaha() {
        return this.numMahasiswa;
    }

    public Dosen getDDosen() {
        return this.dosen;
    }

    public String getdftarMhs() {
        StringBuilder str = new StringBuilder();
        if (this.numMahasiswa == 0) {
            str.append("Belum ada Mahasiswa yang mengambil Mata Kuliah ini");
        }
        else {
            for (int i = 0; i < this.numMahasiswa; i++) {
                str.append(this.daftarMahasiswa[i].toString() + "\n"); 
            }
        }
        
        return str.toString();
    }
    
    // Adder dan drop mahasiswa
    public void addMahasiswa(Mahasiswa mahasiswa) {
        if (this.numMahasiswa == this.kapasitas) {
            System.out.println("Mata kuliah ini sudah penuh");
        }
        else {
            this.daftarMahasiswa[this.numMahasiswa] = mahasiswa;
            this.numMahasiswa++;
        }
    }

    public void dropMahasiswa(Mahasiswa mahasiswa) {
        for (int i = 0; i < this.numMahasiswa; i++) {
            if (daftarMahasiswa[i] == mahasiswa) {
                this.daftarMahasiswa[i] = this.daftarMahasiswa[numMahasiswa-1];
                this.daftarMahasiswa[numMahasiswa-1] = null;
                this.numMahasiswa--;
            }
        }
    }

    // add dan drop dosen
    public void addDosen(Dosen dosen) {
        this.dosen = dosen;
    }

    public void dropDosen() {
        this.dosen = null;
    }

    // Getter dosen
    public String getDosen() {
        if (this.dosen == null){
            return "Belum ada";
        }
        return this.dosen.getNama();
    }

    public String toString() {
        return "";
    }
}