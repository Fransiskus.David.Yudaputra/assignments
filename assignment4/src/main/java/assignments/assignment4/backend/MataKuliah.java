package assignments.assignment4.backend;

public class MataKuliah {
    private String kode;
    private String nama;
    private int sks;
    private int kapasitas;
    private int jumlahMahasiswa;
    private Mahasiswa[] daftarMahasiswa;

    public MataKuliah(String kode, String nama, int sks, int kapasitas){
        this.kode = kode;
        this.nama = nama;
        this.sks = sks;
        this.kapasitas = kapasitas;
        this.jumlahMahasiswa = 0;
        this.daftarMahasiswa = new Mahasiswa[kapasitas];
    }

    public String getdafmas() {
        StringBuilder str = new StringBuilder();
        if (this.jumlahMahasiswa == 0) {
            str.append("Belum ada mahasiswa yang mengambil mata kuliah ini ");
            return str.toString();
        }
        // Sorting elemen daftar Mahasiswa
        for(int i = 0; i < jumlahMahasiswa-1; i++){
            int m = i;
            for(int j = i + 1; j < jumlahMahasiswa; j++){
                if(daftarMahasiswa[m].getNpm() > daftarMahasiswa[j].getNpm()){
                    m = j;
                }       
            }
            // Swapping elemen daftar Mahasiswa
            Mahasiswa temp = daftarMahasiswa[i];
            daftarMahasiswa[i] = daftarMahasiswa[m];
            daftarMahasiswa[m] = temp;
        }
        for (int i = 0; i < this.jumlahMahasiswa; i++) {
            str.append((i + 1) +". "+this.daftarMahasiswa[i].getNama() + " \n"); 
        }
        return str.toString();
    }

    public String getKode(){
        return this.kode;
    }

    public String getNama(){
        return this.nama;
    }
    
    public int getSKS(){
        return this.sks;
    }

    public int getKapasitas(){
        return this.kapasitas;
    }

    public int getJumlahMahasiswa(){
        return this.jumlahMahasiswa;
    }

    public Mahasiswa[] getDaftarMahasiswa(){
        return this.daftarMahasiswa;
    }

    public String toString() {
        return this.nama;
    }

    public void addMahasiswa(Mahasiswa mahasiswa) {
        daftarMahasiswa[jumlahMahasiswa] = mahasiswa;
        this.jumlahMahasiswa += 1;
    }

    public void dropMahasiswa(Mahasiswa mahasiswa) {
        int indeksMahasiswa = cekIndeks(mahasiswa);
        this.jumlahMahasiswa -= 1;
        for(int i=indeksMahasiswa; i<this.jumlahMahasiswa; i++){
            this.daftarMahasiswa[i] = this.daftarMahasiswa[i+1];
        }
    }

    private int cekIndeks(Mahasiswa mahasiswa){
        int indeks = 0;
        for(int i=0; i<this.kapasitas; i++){
            if(this.daftarMahasiswa[i].getNpm() == mahasiswa.getNpm()){
                indeks = i;
                break;
            }
        }
        return indeks;
    }
}
