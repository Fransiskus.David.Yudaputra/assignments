package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class DetailRingkasanMahasiswaGUI {
    public DetailRingkasanMahasiswaGUI(JFrame frame, Mahasiswa mahasiswa, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){
        // Boxing kyta
        JPanel boxing = new JPanel();
        boxing.setLayout(new BoxLayout(boxing, BoxLayout.Y_AXIS));

        // Label title
        boxing.add(Box.createVerticalStrut(50)); 
        JLabel titleLabel = new JLabel();
        titleLabel.setText("Detail Ringkasan Mahasiswa");
        titleLabel.setFont(new Font("Century Gothic", Font.BOLD, 24));
        titleLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        boxing.add(titleLabel, boxing);
        boxing.add(Box.createVerticalStrut(20));

        // label nama
        JLabel namah = new JLabel();
        namah.setText("Nama: " + mahasiswa.getNama());
        namah.setFont(new Font("Century Gothic", Font.PLAIN, 16));
        namah.setAlignmentX(Component.CENTER_ALIGNMENT);
        boxing.add(namah, boxing);
        boxing.add(Box.createVerticalStrut(10));

        // label NPM
        JLabel NPM = new JLabel();
        NPM.setText("NPM: " + mahasiswa.getNpm());
        NPM.setFont(new Font("Century Gothic", Font.PLAIN, 16));
        NPM.setAlignmentX(Component.CENTER_ALIGNMENT);
        boxing.add(NPM, boxing);
        boxing.add(Box.createVerticalStrut(10));

        // label jurusan
        JLabel jurus = new JLabel();
        jurus.setText("Jurusan: " + mahasiswa.extractJurusan(mahasiswa.getNpm()));
        jurus.setFont(new Font("Century Gothic", Font.PLAIN, 16));
        jurus.setAlignmentX(Component.CENTER_ALIGNMENT);
        boxing.add(jurus, boxing);
        boxing.add(Box.createVerticalStrut(10));

        // label Daftar Matkul
        JLabel Dmatkul = new JLabel();
        Dmatkul.setText("Daftar Mata Kuliah: ");
        Dmatkul.setFont(new Font("Century Gothic", Font.PLAIN, 16));
        Dmatkul.setAlignmentX(Component.CENTER_ALIGNMENT);
        boxing.add(Dmatkul, boxing);
        boxing.add(Box.createVerticalStrut(10));

        // label getdaftarmatkul
        JLabel Dematkul = new JLabel();
        Dematkul.setText(mahasiswa.getdaftarmataKuliah());
        Dematkul.setFont(new Font("Century Gothic", Font.BOLD, 16));
        Dematkul.setAlignmentX(Component.CENTER_ALIGNMENT);
        boxing.add(Dematkul, boxing);
        boxing.add(Box.createVerticalStrut(10));

        // label Total sks
        JLabel Totsks = new JLabel();
        Totsks.setText("Total SKS: " + mahasiswa.getTotalSKS());
        Totsks.setFont(new Font("Century Gothic", Font.PLAIN, 16));
        Totsks.setAlignmentX(Component.CENTER_ALIGNMENT);
        boxing.add(Totsks, boxing);
        boxing.add(Box.createVerticalStrut(10));

        // label Hasil cek IRS
        JLabel Cekirs = new JLabel();
        Cekirs.setText("Hasil Pengecekan IRS:");
        Cekirs.setFont(new Font("Century Gothic", Font.PLAIN, 16));
        Cekirs.setAlignmentX(Component.CENTER_ALIGNMENT);
        boxing.add(Cekirs, boxing);
        boxing.add(Box.createVerticalStrut(10));

        // label cek irsnya
        JLabel Cekirss = new JLabel();
        Cekirss.setText(mahasiswa.getirs());
        Cekirss.setFont(new Font("Century Gothic", Font.BOLD, 16));
        Cekirss.setAlignmentX(Component.CENTER_ALIGNMENT);
        boxing.add(Cekirss, boxing);
        boxing.add(Box.createVerticalStrut(10));

        // Button Selesai
        JButton kmb = new JButton("Selesai");
        kmb.setFont(new Font("Century Gothic", Font.PLAIN, 14));
        kmb.setAlignmentX(Component.CENTER_ALIGNMENT);
        kmb.setBackground(new Color(155, 205, 80));
        kmb.setForeground(Color.white);
        kmb.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                frame.getContentPane().removeAll();
                new HomeGUI(frame, daftarMahasiswa, daftarMataKuliah);
            }
        });
        boxing.add(kmb, boxing);

        frame.add(boxing);
        frame.setVisible(true);
    }
}
