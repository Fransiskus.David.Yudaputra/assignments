package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class DetailRingkasanMataKuliahGUI {
    public DetailRingkasanMataKuliahGUI(JFrame frame, MataKuliah mataKuliah, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){
        // Boxing kyta
        JPanel boxing = new JPanel();
        boxing.setLayout(new BoxLayout(boxing, BoxLayout.Y_AXIS));

        // Label title
        boxing.add(Box.createVerticalStrut(50)); 
        JLabel titleLabel = new JLabel();
        titleLabel.setText("Detail Ringkasan Mata Kuliah");
        titleLabel.setFont(new Font("Century Gothic", Font.BOLD, 24));
        titleLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        boxing.add(titleLabel, boxing);
        boxing.add(Box.createVerticalStrut(20));

        // label nama
        JLabel namah = new JLabel();
        namah.setText("Nama mata kuliah: " + mataKuliah.getNama());
        namah.setFont(new Font("Century Gothic", Font.PLAIN, 16));
        namah.setAlignmentX(Component.CENTER_ALIGNMENT);
        boxing.add(namah, boxing);
        boxing.add(Box.createVerticalStrut(10));

        // label kode
        JLabel ngode = new JLabel();
        ngode.setText("Kode: " + mataKuliah.getKode());
        ngode.setFont(new Font("Century Gothic", Font.PLAIN, 16));
        ngode.setAlignmentX(Component.CENTER_ALIGNMENT);
        boxing.add(ngode, boxing);
        boxing.add(Box.createVerticalStrut(10));

        // label SKS
        JLabel sks = new JLabel();
        sks.setText("SKS: " + mataKuliah.getSKS());
        sks.setFont(new Font("Century Gothic", Font.PLAIN, 16));
        sks.setAlignmentX(Component.CENTER_ALIGNMENT);
        boxing.add(sks, boxing);
        boxing.add(Box.createVerticalStrut(10));

        // label jumlah mahaaiswa
        JLabel jms = new JLabel();
        jms.setText("Jumlah Mahasiswa: " + mataKuliah.getJumlahMahasiswa());
        jms.setFont(new Font("Century Gothic", Font.PLAIN, 16));
        jms.setAlignmentX(Component.CENTER_ALIGNMENT);
        boxing.add(jms, boxing);
        boxing.add(Box.createVerticalStrut(10));

        // label kapasitas
        JLabel kapas = new JLabel();
        kapas.setText("Kapasitas: " + mataKuliah.getKapasitas());
        kapas.setFont(new Font("Century Gothic", Font.PLAIN, 16));
        kapas.setAlignmentX(Component.CENTER_ALIGNMENT);
        boxing.add(kapas, boxing);
        boxing.add(Box.createVerticalStrut(10));

        // label Daftar Mahasiswa
        JLabel Dms = new JLabel();
        Dms.setText("Daftar Mahasiswa:");
        Dms.setFont(new Font("Century Gothic", Font.PLAIN, 16));
        Dms.setAlignmentX(Component.CENTER_ALIGNMENT);
        boxing.add(Dms, boxing);
        boxing.add(Box.createVerticalStrut(10));

        // label daftar aslinya broo
        JLabel Dems = new JLabel();
        Dems.setText(mataKuliah.getdafmas());
        Dems.setFont(new Font("Century Gothic", Font.BOLD, 16));
        Dems.setAlignmentX(Component.CENTER_ALIGNMENT);
        boxing.add(Dems, boxing);
        boxing.add(Box.createVerticalStrut(10));

        // Button seelsai
        JButton kmb = new JButton("Selesai");
        kmb.setFont(new Font("Century Gothic", Font.PLAIN, 14));
        kmb.setAlignmentX(Component.CENTER_ALIGNMENT);
        kmb.setBackground(new Color(155, 205, 80));
        kmb.setForeground(Color.white);
        kmb.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                frame.getContentPane().removeAll();
                new HomeGUI(frame, daftarMahasiswa, daftarMataKuliah);
            }
        });
        boxing.add(kmb, boxing);

        frame.add(boxing);
        frame.setVisible(true); 
        
    }
}
