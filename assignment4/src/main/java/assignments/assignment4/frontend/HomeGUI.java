package assignments.assignment4.frontend;

import javax.swing.JFrame;
import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class HomeGUI {
    
    public HomeGUI(JFrame frame, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){
        // Boxing kyta >:D
        JPanel boxing = new JPanel();
        boxing.setLayout(new BoxLayout(boxing, BoxLayout.Y_AXIS));


        // Label
        boxing.add(Box.createVerticalStrut(50)); 
        JLabel titleLabel = new JLabel();
        titleLabel.setText("Selamat Datang di Sistem Akademik");
        titleLabel.setFont(new Font("Century Gothic", Font.BOLD, 24));
        titleLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        boxing.add(titleLabel, boxing);
        boxing.add(Box.createVerticalStrut(20)); 
        

        // Tambah Mahasiswa
        JButton TmbMaha = new JButton("Tambah Mahasiswa");
        TmbMaha.setBackground(new Color(155, 205, 80));
        TmbMaha.setForeground(Color.white);
        TmbMaha.setFont(new Font("Century Gothic", Font.PLAIN, 16));
        TmbMaha.setAlignmentX(Component.CENTER_ALIGNMENT);
        boxing.add(TmbMaha, boxing);
        TmbMaha.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                frame.getContentPane().removeAll();
                new TambahMahasiswaGUI(frame, daftarMahasiswa, daftarMataKuliah);
            }
        });
        boxing.add(Box.createVerticalStrut(10)); 


        // Tambah MataKuliah
        JButton TmbMatkul = new JButton("Tambah Mata Kuliah");
        TmbMatkul.setBackground(new Color(155, 205, 80));
        TmbMatkul.setForeground(Color.white);
        TmbMatkul.setFont(new Font("Century Gothic", Font.PLAIN, 16));
        TmbMatkul.setAlignmentX(Component.CENTER_ALIGNMENT);
        boxing.add(TmbMatkul, boxing);
        TmbMatkul.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                frame.getContentPane().removeAll();
                new TambahMataKuliahGUI(frame, daftarMahasiswa, daftarMataKuliah);
            }
        });
        boxing.add(Box.createVerticalStrut(10));  


        // Tambah IRS
        JButton TmbIRS = new JButton("Tambah IRS");
        TmbIRS.setFont(new Font("Century Gothic", Font.PLAIN, 16));
        TmbIRS.setBackground(new Color(155, 205, 80));
        TmbIRS.setForeground(Color.white);
        TmbIRS.setAlignmentX(Component.CENTER_ALIGNMENT);
        boxing.add(TmbIRS, boxing);
        TmbIRS.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                frame.getContentPane().removeAll();
                new TambahIRSGUI(frame, daftarMahasiswa, daftarMataKuliah);
            }
        });
        boxing.add(Box.createVerticalStrut(10));  

        // Hapus IRS
        JButton HpsIRS = new JButton("Hapus IRS");
        HpsIRS.setFont(new Font("Century Gothic", Font.PLAIN, 16));
        HpsIRS.setBackground(new Color(155, 205, 80));
        HpsIRS.setForeground(Color.white);
        HpsIRS.setAlignmentX(Component.CENTER_ALIGNMENT);
        boxing.add(HpsIRS, boxing);
        HpsIRS.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                frame.getContentPane().removeAll();
                new HapusIRSGUI(frame, daftarMahasiswa, daftarMataKuliah);
            }
        });
        boxing.add(Box.createVerticalStrut(10)); 

        // Ringkasan Mahasiswa
        JButton RksMaha = new JButton("Lihat Ringkasan Mahasiswa");
        RksMaha.setBackground(new Color(155, 205, 80));
        RksMaha.setForeground(Color.white);
        RksMaha.setFont(new Font("Century Gothic", Font.PLAIN, 16));
        RksMaha.setAlignmentX(Component.CENTER_ALIGNMENT);
        boxing.add(RksMaha, boxing);
        RksMaha.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                frame.getContentPane().removeAll();
                new RingkasanMahasiswaGUI(frame, daftarMahasiswa, daftarMataKuliah);
            }
        });
        boxing.add(Box.createVerticalStrut(10));  

        // Ringkasan MataKuliah
        JButton RksMatkul = new JButton("Lihat Ringkasan Mata Kuliah");
        RksMatkul.setBackground(new Color(155, 205, 80));
        RksMatkul.setForeground(Color.white);
        RksMatkul.setFont(new Font("Century Gothic", Font.PLAIN, 16));
        RksMatkul.setAlignmentX(Component.CENTER_ALIGNMENT);
        boxing.add(RksMatkul, boxing);
        RksMatkul.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                frame.getContentPane().removeAll();
                new RingkasanMataKuliahGUI(frame, daftarMahasiswa, daftarMataKuliah);
            }
        });
        boxing.add(Box.createVerticalStrut(10)); 
        boxing.add(Box.createVerticalStrut(10)); 

        frame.add(boxing);
        frame.setVisible(true);
    }
}
