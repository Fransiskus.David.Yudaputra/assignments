package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class RingkasanMahasiswaGUI {

    public RingkasanMahasiswaGUI(JFrame frame, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){
        // Boxing kyta >:D
        JPanel boxing = new JPanel();
        boxing.setLayout(new BoxLayout(boxing, BoxLayout.Y_AXIS));


        // Label
        boxing.add(Box.createVerticalStrut(50)); 
        JLabel titleLabel = new JLabel();
        titleLabel.setText("Ringkasan Mahasiswa");
        titleLabel.setFont(new Font("Century Gothic", Font.BOLD, 24));
        titleLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        boxing.add(titleLabel, boxing);
        boxing.add(Box.createVerticalStrut(20)); 

        // label pilih npm
        JLabel tpm = new JLabel();
        tpm.setText("Pilih NPM");
        tpm.setFont(new Font("Century Gothic", Font.PLAIN, 16));
        tpm.setAlignmentX(Component.CENTER_ALIGNMENT);
        boxing.add(tpm, boxing);
        boxing.add(Box.createVerticalStrut(20));

        // Jcombobox npm
        JComboBox cpm = new JComboBox<String>();
        cpm.setMaximumSize(new Dimension(200,20));
        for (int i = 0; i < daftarMahasiswa.size(); i++) {
            long s = daftarMahasiswa.get(i).getNpm();
            cpm.addItem(Long.toString(s).toString());
        }
        boxing.add(cpm, boxing);
        boxing.add(Box.createVerticalStrut(20));

        // Jbutton lihatlah dan bukalah mata batinmu
        JButton Liat = new JButton("Lihat");
        Liat.setBackground(new Color(155, 205, 80));
        Liat.setForeground(Color.white);
        Liat.setFont(new Font("Century Gothic", Font.PLAIN, 16));
        Liat.setAlignmentX(Component.CENTER_ALIGNMENT);
        boxing.add(Liat, boxing);
        Liat.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                if (cpm.getItemAt(cpm.getSelectedIndex()) == null) {
                    JOptionPane.showMessageDialog(frame, "Mohon isi seluruh Field");
                    return;
                }
                String npmaha = String.valueOf(cpm.getItemAt(cpm.getSelectedIndex()));
                long npmnya = Long.parseLong(npmaha);
                for (Mahasiswa mahasiswa : daftarMahasiswa) {
                    if (mahasiswa.getNpm() == npmnya) {
                        frame.getContentPane().removeAll();
                        new DetailRingkasanMahasiswaGUI(frame, mahasiswa, daftarMahasiswa, daftarMataKuliah);
                    }
                }
            }
        });
        boxing.add(Box.createVerticalStrut(20)); 

        // Button Kembali
        JButton kmb = new JButton("Kembali");
        kmb.setFont(new Font("Century Gothic", Font.PLAIN, 14));
        kmb.setAlignmentX(Component.CENTER_ALIGNMENT);
        kmb.setBackground(new Color(173,196,240));
        kmb.setForeground(Color.white);
        kmb.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                frame.getContentPane().removeAll();
                new HomeGUI(frame, daftarMahasiswa, daftarMataKuliah);
            }
        });
        boxing.add(kmb, boxing);

        frame.add(boxing);
        frame.setVisible(true);
        
    }
}
