package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class TambahIRSGUI {

    public TambahIRSGUI(JFrame frame, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){

        // Boxing kyta
        JPanel boxing = new JPanel();
        boxing.setLayout(new BoxLayout(boxing, BoxLayout.Y_AXIS));

        // Label
        boxing.add(Box.createVerticalStrut(50)); 
        JLabel titleLabel = new JLabel();
        titleLabel.setText("Tambah IRS");
        titleLabel.setFont(new Font("Century Gothic", Font.BOLD, 24));
        titleLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        boxing.add(titleLabel, boxing);
        boxing.add(Box.createVerticalStrut(20));

        // Label npm
        JLabel nLabel = new JLabel();
        nLabel.setText("Pilih NPM");
        nLabel.setFont(new Font("Century Gothic", Font.PLAIN, 14));
        nLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        boxing.add(nLabel, boxing);
        boxing.add(Box.createVerticalStrut(20));
       
        // Jcombobox npm select
        JComboBox cpm = new JComboBox<String>();
        cpm.setMaximumSize(new Dimension(200,20));
        for (int i = 0; i < daftarMahasiswa.size(); i++) {
            long s = daftarMahasiswa.get(i).getNpm();
            cpm.addItem(Long.toString(s).toString());
        }
        boxing.add(cpm, boxing);
        boxing.add(Box.createVerticalStrut(20));

        // label nama matkul 
        JLabel mtk = new JLabel();
        mtk.setText("Pilih Nama Matkul");
        mtk.setFont(new Font("Century Gothic", Font.PLAIN, 14));
        mtk.setAlignmentX(Component.CENTER_ALIGNMENT);
        boxing.add(mtk, boxing);
        boxing.add(Box.createVerticalStrut(20));

        // Jcombobox matkul select
        JComboBox vpm = new JComboBox<String>();
        vpm.setMaximumSize(new Dimension(200,20));
        for (int i = 0; i < daftarMataKuliah.size(); i++) {
            String s = daftarMataKuliah.get(i).getNama();
            vpm.addItem(s.toString());
        }
        boxing.add(vpm, boxing);
        boxing.add(Box.createVerticalStrut(20));

        // Tambahkeun
        JButton tmb = new JButton("Tambahkan");
        tmb.setFont(new Font("Century Gothic", Font.PLAIN, 14));
        tmb.setAlignmentX(Component.CENTER_ALIGNMENT);
        tmb.setBackground(new Color(155, 205, 80));
        tmb.setForeground(Color.white);
        boxing.add(tmb, boxing);
        tmb.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                if (cpm.getItemAt(cpm.getSelectedIndex()) == null || vpm.getItemAt(vpm.getSelectedIndex()) == null) {
                    JOptionPane.showMessageDialog(frame, "Mohon isi seluruh Field");
                    return;
                }
                String npmaha = String.valueOf(cpm.getItemAt(cpm.getSelectedIndex()));
                String naha = String.valueOf(vpm.getItemAt(vpm.getSelectedIndex()));
                long npmnya = Long.parseLong(npmaha);
                for (Mahasiswa mahasiswa : daftarMahasiswa) {
                    if (mahasiswa.getNpm() == npmnya) {
                        for (MataKuliah mataKuliah : daftarMataKuliah){
                            if (mataKuliah.getNama().equals(naha)) {
                                String s = mahasiswa.addMatkul(mataKuliah);
                                JOptionPane.showMessageDialog(frame, s);
                            }
                        }
                    }
                }
            }
        });
        boxing.add(Box.createVerticalStrut(20));

        // Button Kembali
        JButton kmb = new JButton("Kembali");
        kmb.setFont(new Font("Century Gothic", Font.PLAIN, 14));
        kmb.setAlignmentX(Component.CENTER_ALIGNMENT);
        kmb.setBackground(new Color(173,196,240));
        kmb.setForeground(Color.white);
        kmb.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                frame.getContentPane().removeAll();
                new HomeGUI(frame, daftarMahasiswa, daftarMataKuliah);
            }
        });
        boxing.add(kmb, boxing);
        
        frame.add(boxing);
        frame.setVisible(true);
        
    }

    // Uncomment method di bawah jika diperlukan
    /*
    private MataKuliah getMataKuliah(String nama) {

        for (MataKuliah mataKuliah : daftarMataKuliah) {
            if (mataKuliah.getNama().equals(nama)){
                return mataKuliah;
            }
        }
        return null;
    }

    private Mahasiswa getMahasiswa(long npm) {

        for (Mahasiswa mahasiswa : daftarMahasiswa) {
            if (mahasiswa.getNpm() == npm){
                return mahasiswa;
            }
        }
        return null;
    }
    */
}
