package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import javax.swing.JOptionPane;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class TambahMahasiswaGUI{

    public TambahMahasiswaGUI(JFrame frame, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){
       // Boxing kyta
       JPanel boxing = new JPanel();
       boxing.setLayout(new BoxLayout(boxing, BoxLayout.Y_AXIS));

       // Label
       boxing.add(Box.createVerticalStrut(50)); 
       JLabel titleLabel = new JLabel();
       titleLabel.setText("Tambah Mahasiswa");
       titleLabel.setFont(new Font("Century Gothic", Font.BOLD, 24));
       titleLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
       boxing.add(titleLabel, boxing);
       boxing.add(Box.createVerticalStrut(20));

       // Label nama
       JLabel nLabel = new JLabel();
       nLabel.setText("Nama :");
       nLabel.setFont(new Font("Century Gothic", Font.PLAIN, 14));
       nLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
       boxing.add(nLabel, boxing);
       boxing.add(Box.createVerticalStrut(20));

       // Text field nama
       JTextField nama = new JTextField();
       nama.setMaximumSize(new Dimension(200,20));
       boxing.add(nama, boxing);
       boxing.add(Box.createVerticalStrut(20));

       // label npm
       JLabel np = new JLabel();
       np.setText("NPM :");
       np.setFont(new Font("Century Gothic", Font.PLAIN, 14));
       np.setAlignmentX(Component.CENTER_ALIGNMENT);
       boxing.add(np, boxing);
       boxing.add(Box.createVerticalStrut(20));

       // Text field npm
       JTextField npm = new JTextField();
       npm.setMaximumSize(new Dimension(200,20));
       boxing.add(npm, boxing);
       boxing.add(Box.createVerticalStrut(20));
       
       // Button Tambahkeun
       JButton tmb = new JButton("Tambahkan");
       tmb.setFont(new Font("Century Gothic", Font.PLAIN, 14));
       tmb.setAlignmentX(Component.CENTER_ALIGNMENT);
       tmb.setBackground(new Color(155, 205, 80));
       tmb.setForeground(Color.white);
       tmb.addActionListener(new ActionListener(){
           public void actionPerformed(ActionEvent e) {
               if (nama.getText().equals("")|| npm.getText().equals("")) {
                   JOptionPane.showMessageDialog(frame, "Mohon isi seluruh Field");
                   return;
               }
               String namina = nama.getText();
               long npmaha = Long.parseLong(npm.getText());
               for (Mahasiswa mahasiswa : daftarMahasiswa) {
                   if (mahasiswa.getNpm() == npmaha){
                       JOptionPane.showMessageDialog(frame, "NPM " + npmaha + " sudah pernah ditambahkan sebelumnya");
                       npm.setText("");
                       nama.setText("");
                       return;
                   }
               }
               daftarMahasiswa.add(new Mahasiswa (namina,npmaha));
               // Sorting elemen daftar Mahasiswa
               for(int i = 0; i < daftarMahasiswa.size()-1; i++){
                    int m = i;
                    for(int j = i + 1; j < daftarMahasiswa.size(); j++){
                        if(daftarMahasiswa.get(m).getNpm() > daftarMahasiswa.get(j).getNpm()){
                            m = j;
                        }       
                    }
                // Swapping elemen daftar Mahasiswa
                    Mahasiswa temp = daftarMahasiswa.get(i);
                    daftarMahasiswa.set(i, daftarMahasiswa.get(m));
                    daftarMahasiswa.set(m, temp);
                }
               JOptionPane.showMessageDialog(frame, "Mahasiswa " + npm.getText() + "-" + namina + " berhasil ditambahkan");
               npm.setText("");
               nama.setText("");
           }
       });
       boxing.add(tmb, boxing);
       boxing.add(Box.createVerticalStrut(10));

       // Button Kembali
       JButton kmb = new JButton("Kembali");
       kmb.setFont(new Font("Century Gothic", Font.PLAIN, 14));
       kmb.setAlignmentX(Component.CENTER_ALIGNMENT);
       kmb.setBackground(new Color(173,196,240));
       kmb.setForeground(Color.white);
       kmb.addActionListener(new ActionListener(){
           public void actionPerformed(ActionEvent e) {
               frame.getContentPane().removeAll();
               new HomeGUI(frame, daftarMahasiswa, daftarMataKuliah);
           }
       });
       boxing.add(kmb, boxing);

       frame.add(boxing);
       frame.setVisible(true);

    }
    
}
