package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class TambahMataKuliahGUI{

    public TambahMataKuliahGUI(JFrame frame, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){
        
        // Boxing kyta
        JPanel boxing = new JPanel();
        boxing.setLayout(new BoxLayout(boxing, BoxLayout.Y_AXIS));

        // Label title
        boxing.add(Box.createVerticalStrut(50)); 
        JLabel titleLabel = new JLabel();
        titleLabel.setText("Tambah Mata Kuliah");
        titleLabel.setFont(new Font("Century Gothic", Font.BOLD, 24));
        titleLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        boxing.add(titleLabel, boxing);
        boxing.add(Box.createVerticalStrut(20));

        // Label kode
        JLabel nkode = new JLabel();
        nkode.setText("Kode Mata Kuliah :");
        nkode.setFont(new Font("Century Gothic", Font.PLAIN, 14));
        nkode.setAlignmentX(Component.CENTER_ALIGNMENT);
        boxing.add(nkode, boxing);
        boxing.add(Box.createVerticalStrut(10));

        // Textfield kode
        JTextField tkode = new JTextField();
        tkode.setMaximumSize(new Dimension(200,20));
        boxing.add(tkode, boxing);
        boxing.add(Box.createVerticalStrut(10));

        // Label nama
        JLabel lnama = new JLabel();
        lnama.setText("Nama Mata Kuliah :");
        lnama.setFont(new Font("Century Gothic", Font.PLAIN, 14));
        lnama.setAlignmentX(Component.CENTER_ALIGNMENT);
        boxing.add(lnama, boxing);
        boxing.add(Box.createVerticalStrut(10));

        // Textfield nama
        JTextField tnama = new JTextField();
        tnama.setMaximumSize(new Dimension(200,20));
        boxing.add(tnama, boxing);
        boxing.add(Box.createVerticalStrut(10));

        // Label sks
        JLabel lsks = new JLabel();
        lsks.setText("SKS :");
        lsks.setFont(new Font("Century Gothic", Font.PLAIN, 14));
        lsks.setAlignmentX(Component.CENTER_ALIGNMENT);
        boxing.add(lsks, boxing);
        boxing.add(Box.createVerticalStrut(10));

        // Textfield sks
        JTextField tsks = new JTextField();
        tsks.setMaximumSize(new Dimension(200,20));
        boxing.add(tsks, boxing);
        boxing.add(Box.createVerticalStrut(10));

        // label kapasitas
        JLabel lkapa = new JLabel();
        lkapa.setText("Kapasitas :");
        lkapa.setFont(new Font("Century Gothic", Font.PLAIN, 14));
        lkapa.setAlignmentX(Component.CENTER_ALIGNMENT);
        boxing.add(lkapa, boxing);
        boxing.add(Box.createVerticalStrut(10));

        // Textfield kapasitas
        JTextField tkapa = new JTextField();
        tkapa.setMaximumSize(new Dimension(200,20));
        boxing.add(tkapa, boxing);
        boxing.add(Box.createVerticalStrut(10));

        // Button Tambahkeun
        JButton tmb = new JButton("Tambahkan");
        tmb.setFont(new Font("Century Gothic", Font.PLAIN, 14));
        tmb.setAlignmentX(Component.CENTER_ALIGNMENT);
        tmb.setBackground(new Color(155, 205, 80));
        tmb.setForeground(Color.white);
        tmb.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                if (tnama.getText().equals("")|| tkode.getText().equals("") || tkapa.getText().equals("") || tsks.getText().equals("") ) {
                    JOptionPane.showMessageDialog(frame, "Mohon isi seluruh Field");
                    return;
                }
                String namina = tnama.getText();
                String kode = tkode.getText();
                int Ssks = Integer.parseInt(tsks.getText());
                int kapasitass = Integer.parseInt(tkapa.getText());
                for (MataKuliah mataKuliah : daftarMataKuliah) {
                    if (mataKuliah.getNama().equals(namina)){
                        JOptionPane.showMessageDialog(frame, "Mata Kuliah " + namina + " sudah pernah ditambahkan sebelumnya");
                        tnama.setText("");
                        tkode.setText("");
                        tsks.setText("");
                        tkapa.setText("");
                        return;
                    }
                }
                daftarMataKuliah.add(new MataKuliah(kode, namina, Ssks, kapasitass));
                for(int i = 0; i < daftarMataKuliah.size()-1; i++){
                    int m = i;
                    for(int j = i + 1; j < daftarMataKuliah.size();j++){
                        if(daftarMataKuliah.get(m).getNama().compareToIgnoreCase(daftarMataKuliah.get(j).getNama()) > 0 ){
                            m = j;
                        }       
                    }
                    // Swapping elemen daftar Mata Kuliah
                    MataKuliah temp = daftarMataKuliah.get(i);
                    daftarMataKuliah.set(i, daftarMataKuliah.get(m));
                    daftarMataKuliah.set(m, temp);
                }
                JOptionPane.showMessageDialog(frame, "Mata Kuliah " + namina + " berhasil ditambahkan");
                tnama.setText("");
                tkode.setText("");
                tsks.setText("");
                tkapa.setText("");
            }
        });
        boxing.add(tmb, boxing);
        boxing.add(Box.createVerticalStrut(10));

        // Button Kembali
        JButton kmb = new JButton("Kembali");
        kmb.setFont(new Font("Century Gothic", Font.PLAIN, 14));
        kmb.setAlignmentX(Component.CENTER_ALIGNMENT);
        kmb.setBackground(new Color(173,196,240));
        kmb.setForeground(Color.white);
        kmb.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                frame.getContentPane().removeAll();
                new HomeGUI(frame, daftarMahasiswa, daftarMataKuliah);
            }
        });
        boxing.add(kmb, boxing);

        frame.add(boxing);
        frame.setVisible(true);

    }
    
}
